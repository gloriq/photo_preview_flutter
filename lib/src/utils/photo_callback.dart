
typedef OnSuccess = void Function(Object o);
typedef OnError = void Function(String msg);

class PhotoPreviewCallback {
  OnSuccess onSuccess;
  OnError onError;

  PhotoPreviewCallback({required this.onSuccess, required this.onError});
}
