import 'package:flutter/material.dart';
import 'package:photo_preview/src/utils/string_utils.dart';

/// 吐司工具类 默认居中吐司
class ToastUtils {
  /// 吐司
  static void toast(
      {String? msg,
      String? tipImg,
      double? height,
      double? fontSize,
      double? horizontalPadding,
      FontWeight? fontWeight,
      BuildContext? context}) {
    Toast.show(msg, context,
        duration: Toast.LENGTH_SHORT,
        gravity: 2,
        tipImg: tipImg,
        height: height,
        fontSize: fontSize,
        horizontalPadding: horizontalPadding,
        fontWeight: fontWeight);
  }

  ///吐司不覆盖
  static void toastNotCover(
      {String? msg,
      String? tipImg,
      double? height,
      double? fontSize,
      double? horizontalPadding,
      FontWeight? fontWeight,
      BuildContext? context}) {
    Toast.showNotCover(msg, context,
        duration: Toast.LENGTH_SHORT,
        gravity: 2,
        tipImg: tipImg,
        height: height,
        fontSize: fontSize,
        horizontalPadding: horizontalPadding,
        fontWeight: fontWeight);
  }
}

class Toast {
  static final int LENGTH_SHORT = 1;
  static final int LENGTH_LONG = 2;
  static final int BOTTOM = 0;
  static final int CENTER = 1;
  static final int TOP = 2;

  static void show(String? msg, BuildContext? context,
      {int? duration = 2,
      int? gravity = 0,
      Color? backgroundColor = const Color(0xB3000000),
      Color? textColor = Colors.white,
      double? backgroundRadius = 5,
      Border? border,
      String? tipImg,
      double? height,
      double? fontSize = 16.0,
      FontWeight? fontWeight = FontWeight.normal,
      double? horizontalPadding = 15.0}) {
    ToastView.dismiss();
    ToastView.createView(
      msg,
      context,
      duration,
      gravity,
      backgroundColor,
      textColor,
      backgroundRadius,
      border,
      tipImg,
      height,
    );
  }

  static void showNotCover(String? msg, BuildContext? context,
      {int? duration = 2,
      int? gravity = 0,
      Color? backgroundColor = const Color(0xE6000000),
      Color? textColor = Colors.white,
      double? backgroundRadius = 6,
      Border? border,
      String? tipImg,
      double? height,
      double? fontSize = 16.0,
      FontWeight? fontWeight = FontWeight.normal,
      double? horizontalPadding = 15.0}) {
    if (!ToastView._isVisible) {
      ToastView.createView(
        msg,
        context,
        duration,
        gravity,
        backgroundColor,
        textColor,
        backgroundRadius,
        border,
        tipImg,
        height,
      );
    }
  }
}

class ToastView {
  static final ToastView _singleton = new ToastView._internal();

  factory ToastView() {
    return _singleton;
  }

  ToastView._internal();

  static OverlayState? overlayState;
  static OverlayEntry? _overlayEntry;
  static bool _isVisible = false;

  static void createView(
      String? msg,
      BuildContext? context,
      int? duration,
      int? gravity,
      Color? background,
      Color? textColor,
      double? backgroundRadius,
      Border? border,
      String? tipImg,
      double? height) async {
    if (context == null) {
      return;
    }
    overlayState = Overlay.of(context);

    Paint paint = Paint();
    paint.strokeCap = StrokeCap.square;
    paint.color = background!;

    _overlayEntry = new OverlayEntry(
      builder: (BuildContext context) => ToastWidget(
//          widget: Container(
//            width: MediaQuery.of(context).size.width,
//            child: Container(
//                alignment: Alignment.center,
//                width: MediaQuery.of(context).size.width,
//                child: Row(
//                  mainAxisSize: MainAxisSize.min,
//                  children: <Widget>[
//                    Container(
//                      decoration: BoxDecoration(
//                        color: background,
//                        borderRadius: BorderRadius.circular(backgroundRadius),
//                        border: border,
//                      ),
//                      margin: EdgeInsets.symmetric(horizontal: 20),
//                      padding: EdgeInsets.fromLTRB(
//                          14, 10, 14, 10),
//                      child: Row(
//                        mainAxisSize: MainAxisSize.min,
//                        children: <Widget>[
//                          StringUtils.isEmpty(tipImg)
//                              ? Container()
//                              : Image(
//                            image:
//                            AssetImage(tipImg ?? "", package: "vg_base"),
//                            width: 25,
//                            height: 25,
//                          ),
//                          Offstage(
//                            offstage: tipImg == null || tipImg.isEmpty,
//                            child: Container(
//                              width: 10,
//                            ),
//                          ),
//                          Flexible(
//                            child: Text(msg ?? "",
//                                softWrap: true,
//                                maxLines: 2,
//                                overflow: TextOverflow.ellipsis,
//                                style: TextStyle(
//                                  fontSize: 16,
//                                  color: textColor,
//                                  fontWeight: FontWeight.normal,
//                                  decoration: TextDecoration.none,
//                                )),
//                          ),
//                        ],
//                      ),
//                    )
//                  ],
//                )),
//          ),
          widget: Container(
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                color: background,
                borderRadius: BorderRadius.circular(backgroundRadius!),
                border: border,
              ),
              constraints: BoxConstraints(minHeight: height ?? 54),
              margin: EdgeInsets.symmetric(horizontal: 20),
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Visibility(
                    visible: !StringUtils.isEmpty(tipImg),
                    child: Container(
                      margin: EdgeInsets.only(
                          right: (tipImg == null || tipImg.isEmpty) ? 0 : 10),
                      child: Image(
                        image: AssetImage(tipImg ?? "", package: "vg_base"),
                        width: 25,
                        height: 25,
                      ),
                    ),
                  ),
                  Flexible(
                    child: Text(msg ?? "",
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 16,
                          color: textColor,
                          fontWeight: FontWeight.normal,
                          decoration: TextDecoration.none,
                        )),
                  ),
                ],
              ),
            ),
          ),
          gravity: gravity),
    );
    _isVisible = true;
    overlayState!.insert(_overlayEntry!);
    await new Future.delayed(
        Duration(seconds: duration == null ? Toast.LENGTH_LONG : duration));
    dismiss();
  }

  static dismiss() async {
    if (!_isVisible) {
      return;
    }
    _isVisible = false;
    _overlayEntry?.remove();
  }
}

class ToastWidget extends StatelessWidget {
  ToastWidget({
    Key? key,
    @required this.widget,
    @required this.gravity,
  }) : super(key: key);

  final Widget? widget;
  final int? gravity;

  @override
  Widget build(BuildContext context) {
    return new Align(
        alignment:
            gravity == Toast.BOTTOM ? Alignment.center : Alignment.bottomCenter,
        child: widget);
  }
}
