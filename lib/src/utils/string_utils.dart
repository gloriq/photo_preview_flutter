import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:photo_preview/src/utils/toast_utils.dart';

class StringUtils{
  /// isEmpty
  static bool isEmpty(String? text) {
    return text == null || text.isEmpty;
  }

  static bool isNotEmpty(String? text){
    return text != null && text.isNotEmpty;
  }

  /// list转逗号分隔的字符串
  static String listToString(List<String>? list){
    String result = "";
    if(list == null || list.length == 0){
      return "";
    }
    for(String str in list){
      if(isEmpty(result)){
        result = str;
      }else{
        result = result + "," +str;
      }
    }
    return result;
  }

  /// 返回以某个符号分隔的两个字符串拼接显示 包括两个值为空值的判断
  static String getTwoStringBySymbol(String symbol,String? value1, String? value2){
    if(isEmpty(value1) && !isEmpty(value2)){
      return value2!;
    }
    if(isEmpty(value2) && !isEmpty(value1)){
      return value1!;
    }
    if(isEmpty(value2) && isEmpty(value1)){
      return "";
    }
    return value1! + symbol + value2!;
  }

  /// 返回以某个符号分隔的三个字符串拼接显示 包括值为空值的判断 （有没有更好的写法？）
  static String getThreeStringBySymbol(String symbol,String? value1,String? value2,String? value3){
    if(isEmpty(value1) && !isEmpty(value2) && !isEmpty(value3) ){
      return value2! + symbol + value3!;
    }
    if(isEmpty(value2) && !isEmpty(value1) && !isEmpty(value3) ){
      return value1! + symbol + value3!;
    }
    if(isEmpty(value2) && isEmpty(value1) && !isEmpty(value3) ){
      return value3!;
    }
    if(isEmpty(value1) && !isEmpty(value2) && isEmpty(value3) ){
      return value2!;
    }
    if(isEmpty(value2) && !isEmpty(value1) && isEmpty(value3) ){
      return value1!;
    }
    if(!isEmpty(value1) && !isEmpty(value2) && isEmpty(value3) ){
      return value1! + symbol + value2!;
    }
    if(isEmpty(value2) && isEmpty(value1) && isEmpty(value3)){
      return "";
    }
    return value1! + symbol + value2! + symbol + value3!;
  }

  ///获取分割拼串
  static String getSplitStr(List<String?>? list,{String? symbol = "/"}){
    if(list == null || list.isEmpty){
      return "";
    }
    if(symbol == null){
      return "null";
    }
    //移除空
    list.removeWhere((element) => element == null || element.isEmpty);
    return list.join(symbol);
  }

  ///检查是否全部为空
  static bool checkAllEmpty(List<String?>? list){
    list?.removeWhere((element) => element == null || element.isEmpty);
    return list == null || list.isEmpty;
  }

  ///根据List返回第一个不为空的值
  static String getFirstNotEmptyStrByList(List<String>? list){
    if(list == null || list.isEmpty){
      return "";
    }
    for(String item in list){
      if(item != null && item.isNotEmpty){
        return item;
      }
    }
    return "";
  }

  ///设置隐藏分隔符,避免letter、number 避免超出整个截取
  ///跟踪问题：https://github.com/flutter/flutter/issues/18761
  static String? setHiddenSeparatorForStr(String? str){
    if(StringUtils.isEmpty(str)){
      return null;
    }
    return str?.replaceAll("", "\u{200B}");
  }

  ///按长度截断 省略号代替
  static String? subStringAndAppendSymbol(String content,int maxCount,{String? symbol = "..."}){
    if(StringUtils.isEmpty(content)){
      return null;
    }
    if(maxCount == null || maxCount <= 0){
      return content;
    }
    if(content.length > maxCount){
      return content.substring(0,maxCount-1) + (symbol ?? "");
    }
    return content;
  }

  ///判断是否是网络路径
  static bool isNetUrl(String url){
    if(StringUtils.isEmpty(url)){
      return false;
    }
    if(url.startsWith("http://") || url.contains("https://")){
      return true;
    }
    return false;
  }

  ///设置剪切板数据
  static void setClipboardData(BuildContext context,String str){
    if(StringUtils.isEmpty(str)){
      ToastUtils.toast(msg:"空内容",context: context);
      return;
    }
    Clipboard.setData(ClipboardData(text: str)).then((value){
      ToastUtils.toast(msg:"已复制到剪贴板",context: context);
    },onError: (e){
      ToastUtils.toast(msg:"复制到剪贴板错误",context: context);
    }).catchError((e){
      ToastUtils.toast(msg:"复制到剪贴板异常",context: context);
    });
  }

  ///限制中英文个数
  static bool isLimtZHENCharCountForStr(String? content,{int? zhLimitCount, int? enLimitCount}){
    if(StringUtils.isEmpty(content)){
      return true;
    }
    if(zhLimitCount == null || zhLimitCount < 0){
      zhLimitCount = -1;
    }
    if(enLimitCount == null || enLimitCount < 0){
      enLimitCount = -1;
    }
    int _zhCount = 0;
    int _enCount = 0;
    for(int i = 0; i< content!.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        _enCount ++;
      }else{
        _zhCount ++;
      }
    }
    if(zhLimitCount != -1 && _zhCount > zhLimitCount){
      return false;
    }
    if(enLimitCount != -1 && _enCount > enLimitCount){
      return false;
    }
    return true;
  }

  ///限制中英文个数大于
  static bool isMaxLimtCharCountForStr(String? content,{int? maxLimitCount}){
    if(StringUtils.isEmpty(content)){
      return true;
    }
    if(maxLimitCount == null || maxLimitCount < 0){
      maxLimitCount = -1;
    }

    int _zhCount = 0;
    int _enCount = 0;
    for(int i = 0; i< content!.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        _enCount ++;
      }else{
        _zhCount ++;
      }
    }
    // VgLogUtil.v("检验总数字符个数：${_zhCount*2 + _enCount}");
    if(maxLimitCount != -1 && (_zhCount*2 + _enCount) > (maxLimitCount *2)){
      return false;
    }
    return true;
  }

  ///限制中英文个数小于限制
  static bool isMinLimtCharCountForStr(String? content,{int? minLimitCount}){
    if(StringUtils.isEmpty(content)){
      return true;
    }
    if(minLimitCount == null || minLimitCount < 0){
      minLimitCount = -1;
    }

    int _zhCount = 0;
    int _enCount = 0;
    for(int i = 0; i< content!.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        _enCount ++;
      }else{
        _zhCount ++;
      }
    }
    if(minLimitCount != -1 && (_zhCount*2 + _enCount) < (minLimitCount *2)){
      return false;
    }
    return true;
  }

  ///限制中英文个数位数
  static int getMaxLimtCharPositionForStr(String? content,{int? maxLimitCount}){
    final int defaultPosition = -1;
    if(StringUtils.isEmpty(content)){
      return defaultPosition;
    }
    if(maxLimitCount == null || maxLimitCount <= 0){
      return defaultPosition;
    }

    int tmpBytesValue = 0;
    for(int i = 0; i< content!.length ;i++){

      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        tmpBytesValue = tmpBytesValue + 1;
      }else{
        tmpBytesValue = tmpBytesValue + 2;
      }
      if(tmpBytesValue > maxLimitCount*2){
        return i;
      }
    }
    return defaultPosition;

  }

  ///是否包含字符
  static bool isHasChar(String content){
    if(StringUtils.isEmpty(content)){
      return false;
    }
    for(int i = 0; i< content.length ;i++){
      if(content.codeUnitAt(i) <= 126 && content.codeUnitAt(i) >= 27){
        return true;
      }
    }
    return false;
  }


}
