import 'dart:math';
import 'package:flutter/material.dart';
import 'package:photo_preview/src/delegate/photo_preview_video_delegate.dart';
import '../../../photo_preview_export.dart';
import '../../utils/screen_util.dart';

class DefaultPhotoPreviewVideoDelegate extends PhotoPreviewVideoDelegate{
  @override
  bool get enableLoadState => true;

  @override
  bool get enableSlideOutPage => true;

  @override
  GestureConfig initGestureConfigHandler(ExtendedImageState state, BuildContext context, {PhotoPreviewInfoVo? videoInfo}) =>_initGestureConfigHandler(state,context);

  @override
  Widget? videoWidget(PhotoPreviewInfoVo videoInfo, {Widget? result,VideoPlayerController? videoPlayerController,dynamic customVideoPlayerController}) => null;

  @override
  double get controllerBottomDistance => 0.0;

  @override
  EdgeInsetsGeometry? get videoMargin => null;


  @override
  ValueChanged<bool>? get isSlidingStatus => null;

  @override
  ValueChanged<int>? get pageChangeStatus => null;

  @override
  void dispose() => null;

  @override
  void initState() => null;

  ///初始化缩放配置回调
  final Function _initGestureConfigHandler =
      (ExtendedImageState state, context) {
    double initialScale = PhotoPreviewConstant.DEFAULT_INIT_SCALE;

    if (state.extendedImageInfo != null &&
        state.extendedImageInfo!.image != null) {
      initialScale = PhotoPreviewToolUtils.initScale(
          size:
          Size(ScreenUtils.screenW(context), ScreenUtils.screenH(context)),
          initialScale: initialScale,
          imageSize: Size(state.extendedImageInfo!.image.width.toDouble(),
              state.extendedImageInfo!.image.height.toDouble()));
    }
    return GestureConfig(
        inPageView: true,
        initialScale: initialScale,
        minScale: PhotoPreviewConstant.DEFAULT_MIX_SCALE * initialScale,
        maxScale: max(initialScale,
            PhotoPreviewConstant.DEFAULT_MAX_SCALE * initialScale),
        animationMaxScale: max(initialScale,
            PhotoPreviewConstant.DEFAULT_MAX_SCALE * initialScale),
        animationMinScale: min(initialScale,
            PhotoPreviewConstant.DEFAULT_MIX_SCALE * initialScale),
        initialAlignment: InitialAlignment.topCenter,
        //you can cache gesture state even though page view page change.
        //remember call clearGestureDetailsCache() method at the right time.(for example,this page dispose)
        cacheGesture: false);
  };

  @override
  dynamic initCustomVideoPlayerController(PhotoPreviewInfoVo videoInfo, VideoPlayerController videoPlayerController) => null;

}