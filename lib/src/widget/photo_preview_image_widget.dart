import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:photo_preview/src/delegate/photo_preview_image_delegate.dart';
import 'package:photo_preview/src/widget/base_state.dart';
import 'package:photo_preview/src/widget/photo_preview_error_widget.dart';
import '../../photo_preview_export.dart';
import '../utils/screen_util.dart';
import 'inherit/photo_preview_data_inherited_widget.dart';

class PhotoPreviewImageWidget extends StatefulWidget {
  ///图片详情
  final PhotoPreviewInfoVo imageInfo;

  final VoidCallback? popCallBack;

  final int? currentPostion;

  final EdgeInsetsGeometry? imgMargin;

  const PhotoPreviewImageWidget(
      {Key? key,
      required this.imageInfo,
      this.popCallBack,
      this.currentPostion,
      this.imgMargin})
      : super(key: key);

  @override
  PhotoPreviewImageWidgetState createState() =>
      PhotoPreviewImageWidgetState();
}

class PhotoPreviewImageWidgetState extends BaseState<PhotoPreviewImageWidget>
    with TickerProviderStateMixin {
  ///双击缩放控制器
  late AnimationController _doubleClickAnimationController;
  VoidCallback? _doubleClickAnimationListener;
  Animation<double>? _doubleClickAnimation;

  ///手势key
  GlobalKey<ExtendedImageGestureState>? _gestureGlobalKey;

  ///缓存的手势Key
  GlobalKey<ExtendedImageGestureState>? _loadGestureGlobalKey;

  ///图片配置
  PhotoPreviewImageDelegate? _imageDelegate;

  ExtendedImage? _extendedImage;

  @override
  void initState() {
    super.initState();
    _gestureGlobalKey = GlobalKey();
    _loadGestureGlobalKey = GlobalKey();
    _doubleClickAnimationController = AnimationController(
        duration: const Duration(
            milliseconds: PhotoPreviewConstant.DOUBLE_CLICK_SCAL_TIME_MILL),
        vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.imageInfo == null ||
        widget.imageInfo.type == null ||
        widget.imageInfo.type != PhotoPreviewType.image) {
      return Container();
    }
    return _toGestureWidget();
  }

  ///注册点击
  Widget _toGestureWidget() {
    return GestureDetector(
        behavior: HitTestBehavior.translucent,
        onLongPress: (){
          if (_imageDelegate?.onLongPress != null) {
            _imageDelegate?.onLongPress!(
                _gestureGlobalKey?.currentState, widget.imageInfo, context);
            return;
          }
          // _onLongPress(context);
        },
        onTap: () {
          if (_imageDelegate?.onClick != null) {
            _imageDelegate!.onClick!(
                _gestureGlobalKey?.currentState, widget.imageInfo, context);
            return;
          }
          _onClickPop();
        },
        child: Container(
            padding: widget.imgMargin,
            child: Container(
                padding: _imageDelegate?.imgMargin,
                child: _imageDelegate?.imageWidget(widget.imageInfo,
                        result: _toImageWidget()) ??
                    _toImageWidget())));
  }

  ///图片组件
  Widget? _toImageWidget() {
    if (widget.imageInfo.url == null || widget.imageInfo.url!.isEmpty) {
      return PhotoPreviewErrorWidget(_imageDelegate?.enableSlideOutPage);
    }
    if (_extendedImage != null) {
      return _extendedImage;
    }

    if (PhotoPreviewToolUtils.isNetUrl(widget.imageInfo.url)) {
      _extendedImage = ExtendedImage.network(widget.imageInfo.url ?? "",
          //可拖动下滑退出
          enableSlideOutPage: _imageDelegate?.enableSlideOutPage ?? true,
          mode: _imageDelegate?.mode ?? ExtendedImageMode.gesture,
          enableLoadState: _imageDelegate?.enableLoadState ?? true,
          extendedImageGestureKey: _gestureGlobalKey,
          loadStateChanged: (ExtendedImageState state) =>
              _imageDelegate?.loadStateChanged(state,
                  imageInfo: widget.imageInfo) ??
              _toLoadStateChanged(state),
          onDoubleTap: (state) {
            if (_imageDelegate?.onDoubleTap != null) {
              _imageDelegate!.onDoubleTap!(state, widget.imageInfo, context);
              return;
            }
            _onDoubleTap(state);
          },
          initGestureConfigHandler: (state) => _imageDelegate
              !.initGestureConfigHandler(state, imageInfo: widget.imageInfo),
          heroBuilderForSlidingPage: (Widget result) =>
              _imageDelegate!.heroBuilderForSlidingPage(result,
                  imageInfo: widget.imageInfo));
    } else {
      _extendedImage = ExtendedImage.file(File(widget.imageInfo.url!),
          //可拖动下滑退出
          enableSlideOutPage: _imageDelegate?.enableSlideOutPage ?? true,
          mode: _imageDelegate?.mode ?? ExtendedImageMode.gesture,
          onDoubleTap: (state) => (state) {
                if (_imageDelegate?.onDoubleTap != null) {
                  _imageDelegate?.onDoubleTap!(
                      state, widget.imageInfo, context);
                  return;
                }
                _onDoubleTap(state);
              },
          enableLoadState: _imageDelegate?.enableLoadState ?? true,
          extendedImageGestureKey: _gestureGlobalKey,
          initGestureConfigHandler: (state) => _imageDelegate
              !.initGestureConfigHandler(state, imageInfo: widget.imageInfo),
          heroBuilderForSlidingPage: (Widget result) =>
              _imageDelegate!.heroBuilderForSlidingPage(result,
                  imageInfo: widget.imageInfo));
    }
    return _extendedImage;
  }

  ///图片双击回调
  void _onDoubleTap(ExtendedImageGestureState state) {
    double initialScale = PhotoPreviewConstant.DEFAULT_INIT_SCALE;

    if (state.imageGestureConfig?.initialScale != null &&
        state.imageGestureConfig!.initialScale >=
            PhotoPreviewConstant.DEFAULT_INIT_SCALE) {
      initialScale = state.imageGestureConfig!.initialScale;
    }
    final Offset? pointerDownPosition = state.pointerDownPosition;
    final double? begin = state.gestureDetails!.totalScale;
    double end;

    //remove old
    _doubleClickAnimation?.removeListener(_doubleClickAnimationListener!);

    //stop pre
    _doubleClickAnimationController.stop();

    //reset to use
    _doubleClickAnimationController.reset();

    if (begin ==
        PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[0] * initialScale) {
      end = PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[1] * initialScale;
    } else {
      end = PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[0] * initialScale;
    }

    _doubleClickAnimationListener = () {
      //print(_animation.value);
      state.handleDoubleTap(
          scale: _doubleClickAnimation!.value,
          doubleTapPosition: pointerDownPosition);
    };
    _doubleClickAnimation = _doubleClickAnimationController
        .drive(Tween<double>(begin: begin, end: end));

    _doubleClickAnimation!.addListener(_doubleClickAnimationListener!);

    _doubleClickAnimationController.forward();
  }

  ///加载状态
  Widget _toLoadStateChanged(ExtendedImageState state) {
    switch (state.extendedImageLoadState) {
      case LoadState.loading:
        return _toPrelLoadingImageWidget();
        break;
      case LoadState.completed:
        return state.completedWidget;
        break;
      case LoadState.failed:
        return _toPrelLoadingImageWidget();
        break;
    }
    return Container();
  }

  ///预加载已缓存的类型
  Widget _toPrelLoadingImageWidget() {
    // if (widget?.imageInfo?.pLoadingUrl == null ||
    //     widget.imageInfo.pLoadingUrl.isEmpty) {
    //   return null;
    // }
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (){
        if (_imageDelegate?.onLoadingClick != null) {
          _imageDelegate!.onLoadingClick!(
              _loadGestureGlobalKey?.currentState, widget.imageInfo, context);
          return;
        }
        _onClickPop(key: _loadGestureGlobalKey);
      },
      child: ExtendedImage.network(widget.imageInfo.loadingCoverUrl ?? "",
          //可拖动下滑退出
          enableSlideOutPage: _imageDelegate?.enableSlideOutPage ?? true,
          mode: _imageDelegate?.mode ?? ExtendedImageMode.gesture,
          loadStateChanged: (ExtendedImageState state) {
            switch (state.extendedImageLoadState) {
              case LoadState.failed:
                return PhotoPreviewErrorWidget(_imageDelegate?.enableSlideOutPage);
              case LoadState.loading:
                // TODO: Handle this case.
                break;
              case LoadState.completed:
                // TODO: Handle this case.
                break;
            }
            return null;
          },
          extendedImageGestureKey: _loadGestureGlobalKey,
          onDoubleTap: (state) => (state) {
        ///todo: 无法接收到回调
            if (_imageDelegate?.onLoadingDoubleTap != null) {
                  _imageDelegate?.onLoadingDoubleTap!(state, widget.imageInfo, context);
                  return;
                }
                _onDoubleTap(state);
              },
          initGestureConfigHandler: (state) => _imageDelegate
              !.initGestureConfigHandler(state, imageInfo: widget.imageInfo)),
    );
  }

  ///单击退出
  _onClickPop({GlobalKey? key}) {
    if(key == null){
      key = _gestureGlobalKey;
    }
    if (key == null) {
      return;
    }
    ExtendedImageGestureState state = key.currentState as ExtendedImageGestureState;
    if (state == null) {
      return;
    }
    double initialScale = PhotoPreviewConstant.DEFAULT_INIT_SCALE;

    if (state.imageGestureConfig?.initialScale != null &&
        state.imageGestureConfig!.initialScale >=
            PhotoPreviewConstant.DEFAULT_INIT_SCALE) {
      initialScale = state.imageGestureConfig!.initialScale;
    }
    final Offset? pointerDownPosition = state.pointerDownPosition;
    final double? begin = state.gestureDetails!.totalScale;
    double end;

    //remove old
    _doubleClickAnimation?.removeListener(_doubleClickAnimationListener!);

    //stop pre
    _doubleClickAnimationController.stop();

    //reset to use
    _doubleClickAnimationController.reset();

    if (begin ==
        PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[0] * initialScale) {
      if (widget.popCallBack != null) {
        widget.popCallBack!();
      }
      return;
//      end = PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[1] * initialScale;
    } else {
      end = PhotoPreviewConstant.DEFAULT_DOUBLE_TAP_SCALE[0] * initialScale;
    }

    _doubleClickAnimationListener = () {
      //print(_animation.value);
      state.handleDoubleTap(
          scale: _doubleClickAnimation!.value,
          doubleTapPosition: pointerDownPosition);
    };
    _doubleClickAnimation = _doubleClickAnimationController
        .drive(Tween<double>(begin: begin, end: end));

    _doubleClickAnimation!.addListener(_doubleClickAnimationListener!);

    _doubleClickAnimationController.forward();

    if (widget.popCallBack != null) {
      Future.delayed(
          Duration(
              milliseconds: PhotoPreviewConstant.DOUBLE_CLICK_SCAL_TIME_MILL),
          () {
        widget.popCallBack!();
      });
    }
  }


  // _onLongPress(BuildContext context)async{
  //   if (widget.imageInfo.url!.startsWith("http")) {
  //     showModalBottomSheet(
  //         context: context,
  //         backgroundColor: Colors.transparent,
  //         builder: (BuildContext context) {
  //           return Column(
  //             mainAxisAlignment: MainAxisAlignment.end,
  //             children: <Widget>[
  //               Expanded(
  //                 flex: 1,
  //                 child: GestureDetector(
  //                   onTap: () {
  //                     Navigator.pop(context);
  //                   },
  //                   child: Container(
  //                     color: Colors.transparent,
  //                   ),
  //                 ),
  //               ),
  //               GestureDetector(
  //                 child: Container(
  //                   alignment: Alignment.center,
  //                   height: 54.5,
  //                   color: Colors.white,
  //                   child: Text("保存",
  //                       style:
  //                       TextStyle(fontSize: 17, color: Color(0XFF3B3C42))),
  //                 ),
  //                 onTap: () async {
  //                   Navigator.pop(context);
  //                   String? saveResult =
  //                   await SaveUtils.saveImage(url: widget.imageInfo.url);
  //                   toast(saveResult ?? "");
  //                 },
  //               ),
  //               Container(
  //                 color: Color(0XFFF2F3F4),
  //                 height: 10,
  //               ),
  //               GestureDetector(
  //                 child: Container(
  //                   height: 55,
  //                   color: Colors.white,
  //                   alignment: Alignment.center,
  //                   child: Text(
  //                     "取消",
  //                     style: TextStyle(fontSize: 17, color: Color(0XFF3B3C42)),
  //                   ),
  //                 ),
  //                 onTap: () {
  //                   Navigator.of(context).pop();
  //                 },
  //               ),
  //               Container(
  //                 height: ScreenUtils.getBottomBarH(context),
  //                 color: Colors.black,
  //               )
  //             ],
  //           );
  //         });
  //   }
  // }

  @override
  void didChangeDependencies() {
    _imageDelegate = PhotoPreviewDataInherited.of(context).imageDelegate;

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _doubleClickAnimationListener = null;
    _doubleClickAnimationController.dispose();
    _gestureGlobalKey = null;
    _loadGestureGlobalKey = null;
    _extendedImage = null;
    super.dispose();
  }
}
