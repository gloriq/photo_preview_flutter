import 'package:chewie/src/chewie_player.dart';
import 'package:chewie/src/helpers/adaptive_controls.dart';
import 'package:chewie/src/notifiers/index.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photo_preview/src/delegate/photo_preview_video_delegate.dart';
import 'package:photo_preview/src/widget/custom_chewie/custom_video_aspect_ratio_widget.dart';
import 'package:photo_preview/src/widget/inherit/photo_preview_data_inherited_widget.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import 'custom_chewie_widget.dart';

class CustomPlayerWithControls extends StatelessWidget {
  final String? vCoverUrl;

  const CustomPlayerWithControls({Key? key, this.vCoverUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PhotoPreviewVideoDelegate? _videoDelegate = PhotoPreviewDataInherited.of(context).videoDelegate;
    final CustomChewieController chewieController = CustomChewieController.of(context);

    double _calculateAspectRatio(BuildContext context) {
      final size = MediaQuery.of(context).size;
      final width = size.width;
      final height = size.height;

      return width > height ? width / height : height / width;
    }

    Widget _buildControls(
        BuildContext context,
        CustomChewieController chewieController,
        ) {
      return chewieController.showControls
          ? chewieController.customControls ?? const AdaptiveControls()
          : Container();
    }

    Widget _buildPlayerWithControls(
        CustomChewieController chewieController, BuildContext context, PhotoPreviewVideoDelegate? videoDelegate) {
      return Stack(
        children: <Widget>[
          chewieController.placeholder ?? Container(),
          // Center(
          //   child: AspectRatio(
          //     aspectRatio: chewieController.aspectRatio ??
          //         chewieController.videoPlayerController.value.aspectRatio,
          //     child: VideoPlayer(chewieController.videoPlayerController),
          //   ),
          // ),
          CustomVideoAspectRatioWidget(vCoverUrl: vCoverUrl, videoDelegate: videoDelegate),
          chewieController.overlay ?? Container(),
          if (Theme.of(context).platform != TargetPlatform.iOS)
            Consumer<PlayerNotifier>(
              builder: (
                  BuildContext context,
                  PlayerNotifier notifier,
                  Widget? widget,
                  ) =>
                  AnimatedOpacity(
                    opacity: notifier.hideStuff ? 0.0 : 0.8,
                    duration: const Duration(
                      milliseconds: 250,
                    ),
                    child: Container(
                      decoration: const BoxDecoration(color: Colors.black54),
                      child: Container(),
                    ),
                  ),
            ),
          if (!chewieController.isFullScreen)
            _buildControls(context, chewieController)
          else
            SafeArea(
              bottom: false,
              child: _buildControls(context, chewieController),
            ),
        ],
      );
    }

    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          child: _buildPlayerWithControls(chewieController, context, _videoDelegate),
        ),
        Container(
          child: _buildControls(context, chewieController),
        )
      ],
    );

    // return Center(
    //   child: SizedBox(
    //     height: MediaQuery.of(context).size.height,
    //     width: MediaQuery.of(context).size.width,
    //     child: AspectRatio(
    //       aspectRatio: _calculateAspectRatio(context),
    //       child: _buildPlayerWithControls(chewieController, context),
    //     ),
    //   ),
    // );
  }
}
