import 'package:flutter/material.dart';

import '../utils/toast_utils.dart';

typedef DisposeListener = Function();

/// state base
abstract class BaseState<T extends StatefulWidget> extends State<T>
    with WidgetsBindingObserver {
  bool showLoading = false;

  bool _disposed = false;

  List<DisposeListener>? _disposeListeners;

  /// 吐司
  void toast(String msg) {
    ToastUtils.toast(msg: msg, context: context);
  }

  /// 带失败图标的吐司
  void toastFailed(String msg) {
    ToastUtils.toast(
        msg: msg, context: context, tipImg: "images/tishi_fail.png");
  }

  /// 显示空白页
  void showEmptyPage(bool show) {
    // todo 自己实现界面空白页
  }

  /// 显示错误页
  void showErrorPage(bool show) {
    // todo 自己实现界面错误页
  }

  /// 增加dispose listener
  void onDispose(DisposeListener? listener) {
    if (listener == null) {
      return;
    }
    if (_disposeListeners == null) {
      _disposeListeners = [];
    }
    _disposeListeners!.add(listener);
  }

  @override
  void setState(fn) {
    if (_disposed) {
      return;
    }
    super.setState(fn);
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    _disposed = true;
    ///执行DisposeListener
    if (_disposeListeners != null && _disposeListeners!.isNotEmpty)
      for (DisposeListener listener in _disposeListeners!) {
        listener();
      }
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }
}
