import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:photo_preview/photo_preview_export.dart';

import 'custom/common_with_click_hide_custom_class.dart';
import 'delegate/common_with_click_hide_image_delegate.dart';
import 'delegate/common_with_click_hide_slide_delegate.dart';

class CommonWithClickHideStylePage {
  static void navigatorPush(BuildContext context) {

    PhotoPreviewPage.navigatorPush(
        context,
        PhotoPreviewDataSource(initialPage: 0, imgVideoFullList: [
          PhotoPreviewInfoVo(
              url: "https://video.lemontry.com/257dd5f52ec941f7bf1c799aebd33cc0/beadbf2147a940e3a8188ec3b09ab809-6b86cff14b0da8b6d8d337336ac61349-sd.mp4",
              loadingCoverUrl: "http://etpic.we17.com/picture/20210611161444_8631.jpg",
              type: PhotoPreviewType.video),
          PhotoPreviewInfoVo(
              url: "https://s1.ax1x.com/2020/09/17/wR0NmF.jpg",
              loadingCoverUrl: "https://s1.ax1x.com/2020/09/17/wR0NmF.md.jpg",
              type: PhotoPreviewType.image),
          PhotoPreviewInfoVo(
              url: "https://",
              loadingCoverUrl: "https://s1.ax1x.com/2020/09/17/wR3WnI.md.jpg",
              type: PhotoPreviewType.image),
          PhotoPreviewInfoVo(
              url: "https://v-cdn.zjol.com.cn/277001.mp4",
              loadingCoverUrl: "https://s1.ax1x.com/2020/09/17/wR8uCD.jpg",
              type: PhotoPreviewType.video),

          PhotoPreviewInfoVo(
              url: "https://s1.ax1x.com/2020/09/17/wR3H3Q.jpg",
              loadingCoverUrl: "https://s1.ax1x.com/2020/09/17/wR3H3Q.md.jpg",
              type: PhotoPreviewType.image)
        ]),
        customClass: CommonWithClickHideCustomClass(),
        extendedSlideDelegate:
            CommonWithClickHideSlideDelegate(),
        imageDelegate: CommonWithClickHideImageDelegate());
  }
}
