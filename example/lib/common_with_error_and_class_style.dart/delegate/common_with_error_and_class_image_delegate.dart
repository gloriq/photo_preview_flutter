
import 'package:example/common_with_error_and_class_style.dart/custom/common_custom_transimit.dart';
import 'package:photo_preview/photo_preview_export.dart';

class CommonWithErrorAndClassImageDelegate extends DefaultPhotoPreviewImageDelegate {

  late CommonCustomTransmitClass _viewModel;

  @override
  void initState() {
    _viewModel = PhotoPreviewCommonClass.of(context!) as CommonCustomTransmitClass;
    print("测试是否传值成功: ${_viewModel.value}");
  }
}
